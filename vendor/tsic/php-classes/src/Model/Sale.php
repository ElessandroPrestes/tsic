<?php

namespace Tsic\Model;

use \Tsic\DB\Sql;
use \Tsic\Model;
use \Tsic\Model\Product;
use \Tsic\Model\User;

    class Sale extends Model{

        const SESSION = "Sale";

        //Verifica se existe alguma venda na sessão
        public static function getSessionUser()
        {
            $sale = new Sale();

            if(isset($_SESSION[Sale::SESSION]) && (int)$_SESSION[Sale::SESSION]['idsales'] > 0){

                $sale->get((int)$_SESSION[Sale::SESSION]['idsales']);

            }else {

                $sale->getSessionId();

                if(!(int)$sale->getidsale() > 0){

                    $data = [
                        'idsession'=>session_id()
                    ];

                    if (User::checkLogin(false)){

                        $user = User::getSessionUser();

                        $data['iduser'] = $user->getiduser();
                    }

                    $sale->setData($data);

                    $sale->save();

                    $sale->setToSession();

                }

            }

            return $sale;
        }

        public function setToSession()
        {
            $_SESSION[Sale::SESSION] =  $this->getValues();
        }

        public function getSessionId()
        {
            $sql = new Sql();

            $resul = $sql->select("SELECT *
                                   FROM tb_sales
                                   WHERE idsession = :idsession", [
                ':idsession'=>session_id()
            ]);

            if (count($resul) > 0){
                $this->setData($resul[0]);
            }

        }

        //verifica id da venda
        public function get(int $idsales)
        {
            $sql = new Sql();

            $resul = $sql->select("SELECT *
                                   FROM tb_sales
                                   WHERE idsales = :idsales", [
                                    ':idsales'=>$idsales
            ]);

            if (count($resul) > 0){
                $this->setData($resul[0]);
            }

        }


        //Salva vendas no banco de dados
        public function save()
        {
            $sql = new Sql();


            $resul = $sql->select("CALL sp_documents_save(:idsales, :idsession, :vltotal, :status)", [
                                    ':idsales'=>$this->getidsales(),
                                    ':idsession'=>$this->getidsession(),
                                    ':vltotal'=>$this->getvltotal(),
                                    ':status'=>$this->getstatus()
            ]);

            if(count($resul) > 0){
                $this->setData($resul[0]);
            }
        }

        public function addProduct(Product $product)
	    {
                    $sql = new Sql();

                    $sql->query("INSERT INTO tb_salesproducts (idsales, idproduct) VALUES(:idsales, :idproduct)", [
                        ':idsales'=>$this->getidcart(),
                        ':idproduct'=>$product->getidproduct()
                    ]);
                    $this->getCalculateTotal();
	    }


                //Lista todas as vendas
        public static function listSale()
        {
            $sql = new Sql();

            return $sql->select("SELECT * FROM tb_sales where idstatus = 1  ORDER BY idsales desc");

        }

        public static function getPage($page = 1, $itemsPerPage = 5)
        {
            $start = ($page - 1) * $itemsPerPage;

            $sql = new Sql();

            $resul = $sql->select("
                SELECT SQL_CALC_FOUND_ROWS *
                FROM tb_sales
                WHERE idstatus = 1
                ORDER BY idsales desc
                LIMIT $start, $itemsPerPage;
            ");

            $resulTotal = $sql->select("SELECT FOUND_ROWS() AS nrtotal;");
            return [
                'data'=>$resul,
                'total'=>(int)$resulTotal[0]["nrtotal"],
                'pages'=>ceil($resulTotal[0]["nrtotal"] / $itemsPerPage)
            ];
        }

        public function getSearchId($searchId, $page = 1, $itemsPerPage = 5 )
        {
            $start = ($page - 1) * $itemsPerPage;

            $sql = new Sql();

            $resul = $sql->select("
                SELECT SQL_CALC_FOUND_ROWS *
                FROM tb_sales
                WHERE idsales LIKE :search and idstatus =1
                ORDER BY dtregister
                LIMIT $start, $itemsPerPage;
            ", [
                ':search'=>'%'.$searchId.'%'
            ]);

            $resulTotal = $sql->select("SELECT FOUND_ROWS() AS nrtotal;");

            return [
                'data'=>$resul,
                'total'=>(int)$resulTotal[0]["nrtotal"],
                'pages'=>ceil($resulTotal[0]["nrtotal"] / $itemsPerPage)
            ];
        }

        public function getProducts()
        {
            $sql = new Sql();

            return $sql->select("SELECT b.idproduct, b.desproduct, COUNT(*) as qtd, SUM(b.vlprice) AS vltotal
                                    FROM tb_sales a INNER JOIN tb_products b ON a.idproduct = b.idproduct
                                    WHERE a.idsales = :idsales
                                    ORDER BY b.desproduct", [
                                        'idcart'=>$this->getidsales()
                                    ]);
        }

    }
?>
