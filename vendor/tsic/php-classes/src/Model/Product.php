<?php

namespace Tsic\Model;

use \Tsic\DB\Sql;
use \Tsic\Model;

class Product extends Model{

    const ERROR = "ProductError";
    const SUCESS = "ProductSucess";

    public static function listProd()
    {
        $sql = new Sql();

       return  $sql-> select("SELECT * FROM tb_products ORDER BY descproduct ");
    }


    //Salva produto no banco de dados
    public function save()
    {

        $sql = new Sql();

        $resul =   $sql->select("CALL sp_products_save(:idproduct,:codproduct, :descproduct, :vlprice)",array(
                      ":idproduct"=>$this->getidproduct(),
                      ":codproduct"=>$this->getcodproduct(),
                      ":descproduct"=>$this->getdescproduct(),
                      "vlprice"=>$this->getvlprice()
                    ));

         $this->setData($resul[0]);

    }

    public function get($idproduct)
    {
        $sql = new Sql();

       $results =  $sql->select("SELECT *
                                FROM tb_products
                                WHERE idproduct = :idproduct", [
                                ':idproduct'=>$idproduct
        ]);

        $this->setData($results[0]);
    }

    public function delete()
    {
        $sql = new Sql();

        $sql->query("DELETE FROM tb_products WHERE idproduct = :idproduct",[
            ':idproduct'=>$this->getidproduct()
        ]);

    }


        //Paginação
        public static function getPage($page = 1, $itemsPerPage = 5)
        {
            $start = ($page - 1) * $itemsPerPage;

            $sql = new Sql();

            $resul = $sql->select("
                SELECT SQL_CALC_FOUND_ROWS *
                FROM tb_products
                ORDER BY descproduct
                LIMIT $start, $itemsPerPage;
            ");

            $resulTotal = $sql->select("SELECT FOUND_ROWS() AS nrtotal;");
            return [
                'data'=>$resul,
                'total'=>(int)$resulTotal[0]["nrtotal"],
                'pages'=>ceil($resulTotal[0]["nrtotal"] / $itemsPerPage)
            ];
        }

        public function getSearch($search, $page = 1, $itemsPerPage = 5 )
        {
            $start = ($page - 1) * $itemsPerPage;

            $sql = new Sql();

            $resul = $sql->select("
                SELECT SQL_CALC_FOUND_ROWS *
                FROM tb_products
                WHERE codproduct LIKE :search OR descproduct LIKE :search OR vlprice LIKE :search
                ORDER BY descproduct
                LIMIT $start, $itemsPerPage;
            ", [
                ':search'=>'%'.$search.'%'
            ]);

            $resulTotal = $sql->select("SELECT FOUND_ROWS() AS nrtotal;");

            return [
                'data'=>$resul,
                'total'=>(int)$resulTotal[0]["nrtotal"],
                'pages'=>ceil($resulTotal[0]["nrtotal"] / $itemsPerPage)
            ];
        }

        public function getSearchId($searchid, $page = 1, $itemsPerPage = 5 )
        {
            $start = ($page - 1) * $itemsPerPage;

            $sql = new Sql();

            $resul = $sql->select("
                SELECT SQL_CALC_FOUND_ROWS *
                FROM tb_products
                WHERE codproduct LIKE :searchid
                ORDER BY descproduct
                LIMIT $start, $itemsPerPage;
            ", [
                ':searchid'=>'%'.$searchid.'%'
            ]);

            $resulTotal = $sql->select("SELECT FOUND_ROWS() AS nrtotal;");

            return [
                'data'=>$resul,
                'total'=>(int)$resulTotal[0]["nrtotal"],
                'pages'=>ceil($resulTotal[0]["nrtotal"] / $itemsPerPage)
            ];
        }

        public function checkCodeExist($codproduct){

            $sql = new Sql();

            $resul = $sql->select("SELECT *
                                    FROM tb_products
                                    WHERE codproduct = :codproduct", [
                                        ':codproduct'=>$codproduct
                                    ]);

            return (count($resul) > 0);
        }

        public static function setError($msg)
        {
            $_SESSION[Product::ERROR] = $msg;
        }

        public static function getError()
        {
            $msg = (isset($_SESSION[Product::ERROR]) && $_SESSION[Product::ERROR]) ? $_SESSION[Product::ERROR] : '';

            User::clearError();

            return $msg;
        }

        public static function clearError()
	    {
		$_SESSION[Product::ERROR] = NULL;
	    }



}

?>
