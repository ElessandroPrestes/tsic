<?php

    namespace Tsic\Model;

    use \Tsic\DB\Sql;
    use \Tsic\Model;

    class User extends Model{

        const SESSION = "User";
        const SECRET = "tsic_secret";
        const ERROR = "UserError";
        const ERROR_REGISTER = "UserErrorRegister";
        const SUCCESS = "UserSucesss";



        //Verifica se tem alguma sessão ativa
        public static function getSessionUser()
        {
            $user = new User();

            if(isset($_SESSION[User::SESSION]) && (int)$_SESSION[User::SESSION]['iduser'] > 0)
            {
                $user->setData($_SESSION[User::SESSION]);
            }
            return $user;
        }


        //Identifica se o usuario é admin ou vendedor
        public static function checkLogin($admin = true){

            if(!isset($_SESSION[User::SESSION]) ||

                      !$_SESSION[User::SESSION] ||

                      !(int)$_SESSION[User::SESSION]["iduser"] > 0
                      )
            {
                return false;

            }else {

                if($admin === true && (bool)$_SESSION[User::SESSION]['admin'] === true){

                    return true;

                }else if($admin === false){

                    return true;

                }else{

                    return false;

                }
            }
        }

        public static function login($login, $password)
        {
            $sql = new Sql();

            $resul =  $sql->select("SELECT *
                                    FROM tb_users a
                                    INNER JOIN tb_persons b ON a.idperson = b.idperson
                                    WHERE a.deslogin = :LOGIN", array(
                                    ":LOGIN"=>$login
            ));


            if(count($resul) === 0)
            {
                throw new \Exception("Usuário inexistente ou senha inválida");
            }

            $data = $resul[0];

            if(password_verify($password, $data["despassword"])===true)
            {
                $user = new User();

                $data['desperson'] = utf8_encode($data['desperson']);

                $user->setData($data);

                $_SESSION[User::SESSION] = $user->getValues();

                return $user;

            }else{
                throw new \Exception("Usuário inexistente ou senha inválida");
            }
        }


        public static function verifyLogin($admin = true)
        {
            if(!User::checkLogin($admin)){
                if($admin){
                    header("Location:/admin/login");
                }else{
                    header("Location:/login");
                }
                exit;
            }
        }

        public static function setError($msg)
        {
            $_SESSION[User::ERROR] = $msg;
        }

        public static function getError()
        {
            $msg = (isset($_SESSION[User::ERROR]) && $_SESSION[User::ERROR]) ? $_SESSION[User::ERROR] : '';
            User::clearError();
            return $msg;
        }

        public static function clearError()
        {
            $_SESSION[User::ERROR] = NULL;
        }

        public static function setSuccess($msg)
        {
            $_SESSION[User::SUCCESS] = $msg;
        }
        public static function getSuccess()
        {
            $msg = (isset($_SESSION[User::SUCCESS]) && $_SESSION[User::SUCCESS]) ? $_SESSION[User::SUCCESS] : '';
            User::clearError();
            return $msg;
        }
        public static function clearSuccess()
        {
            $_SESSION[User::SUCCESS] = NULL;
        }



        public static function logout(){
            $_SESSION[User::SESSION] = NULL;
        }

        public static function listUser(){

            $sql = new Sql();

            return  $sql-> select("SELECT *
                                    FROM tb_users a
                                    INNER JOIN tb_persons b USING(idperson)
                                    ORDER BY a.iduser");

        }

        public function setPassword($password)
        {
            $sql = new Sql();

            $sql->query("UPDATE tb_users SET despassword = :password WHERE iduser = :iduser", array(
                ":password"=>$password,
                ":iduser"=>$this->getiduser()
            ));
        }

        //Verifica se existe algum login ja cadastrado no banco de dados
        public static function checkLoginExist ($login)
        {
            $sql = new Sql();

            $resul = $sql->select("SELECT *
                                    FROM tb_users
                                    WHERE deslogin = :deslogin", [
                                    ':deslogin'=>$login
                                    ]);
            return (count($resul) > 0);
        }
        //Verifica se existe algum login ja cadastrado no banco de dados
        public static function checkEmailExist ($email)
        {
            $sql = new Sql();

            $resul = $sql->select("SELECT *
                                    FROM tb_persons
                                    WHERE desemail = :desemail", [
                                    ':desemail'=>$email
                                    ]);
            return (count($resul) > 0);
        }

        public static function getPasswordHash($password)
        {
            return password_hash($password, PASSWORD_DEFAULT, [
                'cost'=>12
            ]);
        }


        public static function getPage($page =1, $itemsPerPage = 5)
        {
            $start = ($page - 1 ) * $itemsPerPage;

            $sql = new Sql();

            $resul = $sql->select("SELECT SQL_CALC_FOUND_ROWS *
                                       FROM tb_users a
                                       INNER JOIN tb_persons b USING(idperson)
                                       ORDER BY b.desperson
                                       LIMIT $start, $itemsPerPage;

            ");

            $resulTotal = $sql->select("SELECT FOUND_ROWS() AS nrtotal;");

            return [
                'data'=>$resul,
                'total'=>(int)$resulTotal[0]["nrtotal"],
                'pages'=>ceil($resulTotal[0]["nrtotal"] / $itemsPerPage)
            ];
        }
        public static function getPageSearch($search, $page =1, $itemsPerPage = 8)
        {
            $start = ($page - 1 ) * $itemsPerPage;

            $sql = new Sql();

            $resul = $sql->select("SELECT SQL_CALC_FOUND_ROWS *
                                     FROM tb_users a
                                     INNER JOIN tb_persons b USING(idperson)
                                     WHERE b.desperson LIKE :search OR b.desemail OR a.deslogin LIKE :search
                                     ORDER BY b.desperson
                                     LIMIT $start, $itemsPerPage;

            ", [
                ':search'=>'%'.$search.'%'
            ]);

            $resulTotal = $sql->select("SELECT FOUND_ROWS() AS nrtotal;");

            return [
                'data'=>$resul,
                'total'=>(int)$resulTotal[0]["nrtotal"],
                'pages'=>ceil($resulTotal[0]["nrtotal"] / $itemsPerPage)
            ];
        }

        public function save(){

            $sql = new Sql();


          $resul =   $sql->select("CALL sp_users_save(:desperson, :deslogin, :despassword,
                        :desemail, :nrphone, :admin)", array(
                          ":desperson"=>$this->getdesperson(),
                          ":deslogin"=>$this->getdeslogin(),
                          ":despassword"=>password_hash($this->getdespassword(), PASSWORD_DEFAULT, [ "cost" => 12 ] ),
                          ":desemail"=>$this->getdesemail(),
                          ":nrphone"=>$this->getnrphone(),
                          ":admin"=>$this->getadmin()
                        ));

        $this->setData($resul[0]);


        }

        public function get($iduser)
        {

            $sql = new Sql();

            $resul = $sql->select("SELECT *
                                    FROM tb_users a INNER JOIN tb_persons b USING(idperson)
                                    WHERE a.iduser = :iduser", array(
                                    ":iduser"=>$iduser
            ));

            $data = $resul[0];

            $data['desperson'] = utf8_encode($data['desperson']);

            $this->setData($data);

         }

         public function update()
         {

             $sql = new Sql();

             $resul =  $sql->select("CALL sp_usersupdate_save(:iduser, :desperson, :deslogin,
                                      :despassword, :desemail, :nrphone, :admin)", array(
                             ":iduser"=>$this->getiduser(),
                             ":desperson"=>$this->getdesperson(),
                             ":deslogin"=>$this->getdeslogin(),
                            ":despassword" => password_hash( $this->getdespassword(), PASSWORD_DEFAULT, [ "cost" => 12 ] ),
                             ":desemail"=>$this->getdesemail(),
                             ":nrphone"=>$this->getnrphone(),
                             ":admin"=>$this->getadmin()
                           ));


           $this->setData($resul[0]);

         }

         public function delete()
         {
             $sql = new Sql();

             $sql->query("CALL sp_users_delete(:iduser)", array(

                 ":iduser"=>$this->getiduser()

             ));
         }








    }

?>
