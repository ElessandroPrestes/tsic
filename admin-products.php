<?php

    use \Tsic\PageAdmin;
    use \Tsic\Model\User;
    use \Tsic\Model\Product;


    $app->get("/admin/products", function(){

        User::verifyLogin();

        $search = (isset($_GET['search'])) ? $_GET['search']: "";

        $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;

        if ($search != '') {
            $pagination = Product::getSearch($search, $page);
        } else {
            $pagination = Product::getPage($page);
        }
        $pages = [];

        for ($x = 0; $x < $pagination['pages']; $x++)
        {
            array_push($pages, [
                'href'=>'/admin/products?'.http_build_query([
                    'page'=>$x+1,
                    'search'=>$search
                ]),
                'text'=>$x+1
            ]);
        }
        $products = Product::listProd();

        $page = new PageAdmin();

        $page->setTpl("products", [
            "products"=>$pagination['data'],
            "search"=>$search,
            "pages"=>$pages
        ]);


    });

    //Cadastra produto
    $app->get("/admin/products/create", function(){

        User::verifyLogin();

        $page = new PageAdmin();

        $page->setTpl("products-create", array(
            "error"=>Product::getError()
        ));
    });

    $app->post("/admin/products/create", function(){

        User::verifyLogin();

        $prod = new Product();

        if(Product::checkCodeExist($_POST['codproduct']) === true){

            Product::setError("Este código do produto já esta cadastrado");
            header("Location: /admin/products/create");

        }

        $prod->setData($_POST);

        $prod->save();

        header("Location: /admin/products");
        exit;
    });

    //Edita Produto
    $app->get("/admin/products/:idproduct", function($idproduct){

        User::verifyLogin();

        $product = new Product();

        $product->get((int)$idproduct);

        $page = new PageAdmin();

        $page->setTpl("products-update",[
            'product'=>$product->getValues()
        ]);
    });

    $app->post("/admin/products/:codproduct", function($idproduct){

        User::verifyLogin();

        $product = new Product();

        $product->get((int)$idproduct);

        $product->setData($_POST);

        $product->save();

        header('Location: /admin/products');
        exit;
    });

    //Deleta produto
    $app->get("/admin/products/:idproduct/delete", function($idproduct){

        User::verifyLogin();

        $product = new Product();

        $product->get((int)$idproduct);

        $product->delete();

        header('Location: /admin/products');
        exit;
    });






?>
