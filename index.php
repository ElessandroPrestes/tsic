<?php
session_start();

require_once("vendor/autoload.php");

use \Slim\Slim;

$app = new Slim();

$app->config('debug', true);

require_once("admin.php");
require_once("admin-login.php");
require_once("admin-users.php");
require_once("admin-products.php");
require_once("admin-sales.php");
require_once("loja.php");
require_once("loja-login.php");
require_once("loja-products.php");
require_once("loja-sales.php");
require_once("functions.php");

$app->run();

 ?>
