<?php


use \Tsic\Page;
use \Tsic\Model\User;
use \Tsic\Model\Product;
use \Tsic\Model\Sale;



 $app->get("/products", function(){

  $searchid = (isset($_GET['searchid'])) ? $_GET['searchid']: "";
    $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;

    if ($searchid != '') {
		$pagination = Product::getSearchId($searchid, $page);
	} else {
		$pagination = Product::getPage($page);
	}
    $pages = [];

    for ($x = 0; $x < $pagination['pages']; $x++)
	{
		array_push($pages, [
			'href'=>'/products?'.http_build_query([
				'page'=>$x+1,
				'searchid'=>$searchid
			]),
			'text'=>$x+1
		]);
	}
    $products = Product::listProd();

    $page = new Page();

    $page->setTpl("products", [
		  "products"=>$pagination['data'],
		  "searchid"=>$searchid,
		  "pages"=>$pages
    ]);

});


?>
