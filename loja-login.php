<?php

use \Tsic\Page;
use \Tsic\Model\User;

$app->get("/login", function(){

    $page = new Page([
          "header"=>false,
          "footer"=>false
      ]);
  
  
      $page->setTpl("login", [
          'error'=>User::getError()
      ]);
   });
  
   $app->post("/login", function(){
  
      try{
  
          User::login($_POST['login'], $_POST['password']);
  
      }catch(Exception $e){
  
          User::setError($e->getMessage());
      }
      header("Location: / ");
      exit;
  
   });
  
   $app->get('/logout', function(){
  
      User::logout();
  
      header("Location:/login");
      exit;
  
  });
  


?>