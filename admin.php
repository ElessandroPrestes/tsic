<?php

    use \Tsic\PageAdmin;
    use \Tsic\Model\User;
	use \Tsic\Model\Product;



$app->get('/admin', function() {

    User::verifyLogin();

    $user = new User();

	$page = new PageAdmin();

	$page->setTpl("index");
});

?>
