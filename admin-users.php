<?php

use \Tsic\PageAdmin;
use \Tsic\Model\User;

$app->get("/admin/users/:iduser/password", function($iduser){

	User::verifyLogin();

    $user = new User();

	$user->get((int)$iduser);

	$page = new PageAdmin();

	$page->setTpl("users-password", [
		"user"=>$user->getValues(),
		"msgError"=>User::getError(),
		"msgSuccess"=>User::getSuccess()
    ]);

});
$app->post("/admin/users/:iduser/password", function($iduser){

	User::verifyLogin();

        if(!isset($_POST['despassword']) || $_POST['despassword']===''){
            User::setError("Preeencha a nova senha");
            header("Location: /admin/users/$iduser/password");
            exit;
        }
        else if(!isset($_POST['despassword-confirm']) || $_POST['despassword-confirm']===''){
            User::setError("Preeencha a confirmação da nova senha");
            header("Location: /admin/users/$iduser/password");
            exit;
        }
        else if($_POST['despassword'] !== $_POST['despassword-confirm']){
            User::setError("Confirme corretamente as senhas");
            header("Location: /admin/users/$iduser/password");
            exit;
        }
        else {
            $user = new User();

            $user->get((int)$iduser);
            //Salva a nova senha já passando o hash usando o metodo
            $user->setPassword(User::getPasswordHash($_POST['despassword']));

            User::setSuccess("Senha alterada com sucesso.");
            header("Location: /admin/users/$iduser/password");
            exit;
        }

});


$app->get("/admin/users", function(){

    User::verifyLogin();

	$search = (isset($_GET['search'])) ? $_GET['search'] : "";

	$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;

	if($search != ''){
		$pagination = User::getPageSearch($search, $page);
	}else {
		$pagination = User::getPage($page);
	}

	$pages = [];

	for($x = 0; $x < $pagination['pages']; $x++)
	{
		array_push($pages,[
			'href'=>'/admin/users?'.http_build_query([
				'page'=>$x+1,
				'search'=>$search
			]),
			'text'=>$x+1
		]);
	}
	$page = new PageAdmin();

	$page->setTpl("users", array(
		"users"=>$pagination['data'],
		"search"=>$search,
		"pages"=>$pages
	));

});



$app->get("/admin/users/create", function(){

	User::verifyLogin();

	$page = new PageAdmin();

    $page->setTpl("users-create", array(
        "msgError"=>User::getError(),
		"msgSuccess"=>User::getSuccess()
    ));

});

//Salva os dados do usuario no banco de dados
$app->post("/admin/users/create", function(){

	User::verifyLogin();

    $user =  new User();

    if (User::checkEmailExist($_POST['desemail']) === true) {

        User::setError("Este endereço de e-mail já está cadastrado.");
        header("Location: /admin/users/create");
        exit;

    }

    $_POST["admin"] = (isset($_POST["admin"])) ? 1 : 0;

    $user->setData($_POST);

    $user->save();

    User::setSuccess("Cadastro Efetuado com sucesso.");

	header("Location:/admin/users/create");
	exit;




});

$app->get("/admin/users/:iduser/delete", function($iduser){
	User::verifyLogin();

	$user = new User();

	$user->get((int)$iduser);

	$user->delete();

	header("Location: /admin/users");
	exit;

});


//Rota para editar os usuários(UPDATE)
$app->get("/admin/users/:iduser", function($iduser){

	User::verifyLogin();

	$user = new User();

	$user->get((int)$iduser);

	$page = new PageAdmin();

	$page ->setTpl("users-update", array(
        "user"=>$user->getValues()
    ));

});


//Rota para salvar os dados do usuário
$app->post("/admin/users/:iduser", function($iduser){

	User::verifyLogin();

	$user = new User();

	$_POST["admin"] = (isset($_POST["admin"]))?1:0;

	$user->get((int)$iduser);

	$user->setData($_POST);

	$user->update();

	header("Location: /admin/users");
	exit;
});







?>
