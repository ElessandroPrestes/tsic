<?php

    use \Tsic\PageAdmin;
    use \Tsic\Model\User;
	use \Tsic\Model\Product;
	use \Tsic\Model\Sale;



$app->get("/admin/sales", function(){

    User::verifyLogin();

    $searchId = (isset($_GET['search'])) ? $_GET['search']: "";
    $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;

    if ($searchId != '') {
        $pagination = Sale::getSearchId($searchId, $page);
    } else {
        $pagination = Sale::getPage($page);
    }
    $pages = [];

    for ($x = 0; $x < $pagination['pages']; $x++)
    {
        array_push($pages, [
            'href'=>'/admin/sales?'.http_build_query([
                'page'=>$x+1,
                'search'=>$searchId
            ]),
            'text'=>$x+1
        ]);
    }
    $sales = Sale::listSale();

    $page = new PageAdmin();

    $page->setTpl("sales", [
        "sales"=>$pagination['data'],
        "search"=>$searchId,
        "pages"=>$pages
    ]);


});

?>
