
import gulp from 'gulp';
import watch from 'gulp-watch';
import concat from'gulp-concat-css';
import conjs from'gulp-concat';
import notify from'gulp-notify';
import css from'gulp-clean-css';
import uglify from 'gulp-uglify-es';
import rename from 'gulp-rename';

gulp.task('concss', () =>{
    return gulp.src('res/default/assets/css/*.css')
    .pipe(concat("style.con.css"))
    .on("error", notify.onError("Error: <%= error.message %>"))
    .pipe(gulp.dest('res/default/assets/css/concat/'));
});

gulp.task('mincss', () =>{
  return gulp.src('res/default/assets/css/concat/style.con.css')
  .pipe(rename('style.min.css'))
  .pipe(css())
  .on("error", notify.onError("Error: <%= error.message %>"))
  .pipe(gulp.dest('res/default/assets/css/minify/'));
});

gulp.task('conjs', () => {
  return gulp.src(['res/default/assets/js/jquery.js.' ,'res/default/assets/js/bootstrap.js', 'res/defau;t/assets/js/app.js'])
  .pipe(conjs("main.con.js"))
  .on("error", notify.onError("Error: <%= error.message %>"))
  .pipe(gulp.dest('res/default/assets/js/concat/'));
});

gulp.task('minjs', () =>{
  return gulp.src('res/default/assets/js/concat/main.con.js')
  .pipe(rename('main.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('res/default/assets/js/minify/'))
});

gulp.task('default',['concss','mincss','minjs'], () => {
    gulp.watch('assets/css/*css', ['concss']);
    gulp.watch('dist/css/concat/style.css', ['mincss']);
    gulp.watch('js/index.js', ['minjs']);
});